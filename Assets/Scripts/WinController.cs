﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinController : MonoBehaviour {

    public Button reset;
    public Button exit;
	 void OnCollisionEnter(Collision collision)
    {
        Destroy(collision.gameObject);
        Time.timeScale = 0f;
        reset.gameObject.SetActive(true);
        exit.gameObject.SetActive(true);
        
    }
}
