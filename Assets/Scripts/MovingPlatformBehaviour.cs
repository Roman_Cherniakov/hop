﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformBehaviour : MonoBehaviour {
        
        public float movespeed = 20;
        public float speed = 10;
        bool mflag=false;
        float minDst=0.05f;
        float end_position;
        float end_position1;

       void Start() {
        if (transform.position.x >= 6.5 && transform.position.x <= 7)
        {
            mflag = true;
            end_position = Random.Range(10f,13.5f);
            end_position1 = transform.position.x;
        }
           else if(transform.position.x >=7 && transform.position.x <= 8)
        {
            mflag = true;
            end_position = Random.Range(11f, 13.5f);
            end_position1 = transform.position.x;
        }
        else if (transform.position.x >= 8 && transform.position.x <= 9)
        {
            mflag = true;
            end_position = Random.Range(12f, 13.5f);
            end_position1 = transform.position.x;
        }
        else if (transform.position.x >= 13 && transform.position.x <= 13.5)
        {
            end_position = transform.position.x;
            end_position1 = Random.Range(6.5f, 10f);
        }
        else if (transform.position.x >= 13 && transform.position.x <= 12)
        {
            end_position = transform.position.x;
            end_position1 = Random.Range(6.5f, 9f);
        }
        else
        {
            end_position = transform.position.x;
            end_position1 = Random.Range(6.5f, 8f);
        }

       }

       void FixedUpdate()
       {
           if (mflag)
           {
               this.transform.Translate(new Vector3(0, 0.0f, -1) * movespeed * Time.deltaTime);
               this.transform.Translate(new Vector3(1, 0.0f, 0) * speed * Time.deltaTime);
               if (Mathf.Abs(transform.position.x - end_position) < minDst)
               {
                   mflag = !mflag;
               }
           }
           else
           {
               this.transform.Translate(new Vector3(0, 0.0f, -1) * movespeed * Time.deltaTime);
               this.transform.Translate(new Vector3(-1, 0.0f, 0) * speed * Time.deltaTime);
               if (Mathf.Abs(transform.position.x - end_position1) < minDst)
               {
                   mflag = !mflag;
               }
           }

        }
}
