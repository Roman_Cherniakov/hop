﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonRestart_Click : MonoBehaviour {

    public void Button_Click()
    {
        SceneManager.LoadScene("Game", LoadSceneMode.Single);
    }

}
