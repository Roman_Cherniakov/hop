﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartScript : MonoBehaviour {

    public Button resetb;
    public Button exitb; 

	void Start () {
        resetb.gameObject.SetActive(false);
        exitb.gameObject.SetActive(false);
	}
	
}
