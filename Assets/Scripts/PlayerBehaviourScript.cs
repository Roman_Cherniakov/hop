﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBehaviourScript : MonoBehaviour
{
    public GameObject pl;
    public GameObject movingplatform;
    public float speed = 10f;
    public float changeSpeed =2f;
    int i = 0;
    Rigidbody rg;
    Vector3 movementVector;
    Vector3 position;
    float offset;
    public Text score;
    public Text record;
    float timespeed = 2;
    int _score;

    void Start()
    {
        rg = GetComponent<Rigidbody>();
        
        pl.transform.localScale = new Vector3(2.7f, 0.2f, 2.7f);
        movingplatform.transform.localScale = new Vector3(2.7f, 0.2f, 2.7f);
        score.text = "0";
        _score = PlayerPrefs.GetInt("Record");
        record.text = "Record: " + _score;
        Time.timeScale = 0;
        this.transform.position = new Vector3(10f, 6f, 0f);
        position = transform.position;
    }
    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Time.timeScale = timespeed;
        }

       
    }
    void FixedUpdate()
    {       
        transform.Rotate(new Vector3(20, 0, 0)* 15f * Time.deltaTime);
        Debug.Log(Input.GetTouch(0).position.x);
       

    }

    void LateUpdate()
    {
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            offset = transform.position.x - (Input.GetTouch(0).position.x/100+6);
        }
        else if (Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            transform.position = new Vector3(Input.GetTouch(0).position.x/100+6+offset, transform.position.y, transform.position.z);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("up"))
        {   
            rg.velocity = Vector3.zero;
            //this.transform.position = new Vector3(this.position.x, 0.02f, 0);
           // movementVector = position*0.89f;
            // movementVector = *0.89f;
            rg.AddForce(new Vector3(0, 6f, 0f) * 0.91f * speed);
            i++;
            int random = Random.Range(1,10);
            if (i < 100)
            {
                if (random >= 3)
                {
                    Instantiate(pl);
                    pl.transform.position = new Vector3(Random.Range(6.5f, 13.5f), -0.6f, 27);
                }
                else if (random == 1)
                {
                    movingplatform.transform.position = new Vector3(Random.Range(6.5f, 8f), -0.6f, 27);
                    Instantiate(movingplatform);
                }
                else
                {
                    movingplatform.transform.position = new Vector3(Random.Range(10f, 13.5f), -0.6f, 27);
                    Instantiate(movingplatform);
                }
            }
            else
            {
                if (random == 1 || random == 2)
                {
                    movingplatform.transform.position = new Vector3(Random.Range(6.5f, 8f), -0.6f, 27);
                    Instantiate(movingplatform);
                }
                else if(random == 3 || random == 4)
                {
                    movingplatform.transform.position = new Vector3(Random.Range(10f, 13.5f), -0.6f, 27);
                    Instantiate(movingplatform);
                }
                else
                {
                    Instantiate(pl);
                    pl.transform.position = new Vector3(Random.Range(6.5f, 13.5f), -0.6f, 27);
                }
            }
            if (i >= 10 && i <= 20 || i >= 40 && i <= 45)
            {
                pl.transform.localScale = new Vector3(pl.transform.localScale.x - 0.05f, pl.transform.localScale.y, pl.transform.localScale.z - 0.05f);
                movingplatform.transform.localScale = new Vector3(pl.transform.localScale.x - 0.05f, pl.transform.localScale.y, pl.transform.localScale.z - 0.05f);
            }

            if ((i > 9 && i < 20) || (i > 43 && i < 55) || (i > 79 && i < 95) || (i > 100 && i < 120))
                if (Time.timeScale < 4)
                {
                    Time.timeScale += 0.1f;
                    timespeed += 0.1f;
                }          
            if(i>_score)
            {
                record.text = "NEW RECORD!";
                PlayerPrefs.SetInt("Record", i);
            }
            score.text = i.ToString();
                       
        }
    }
}
