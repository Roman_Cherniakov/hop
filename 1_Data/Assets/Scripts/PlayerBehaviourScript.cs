﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBehaviourScript : MonoBehaviour
{
    public GameObject pl;
    public GameObject movingplatform;
    public float speed = 10f;
    public float changeSpeed =2f;
    int i = 0;
    Rigidbody rg;
    Vector3 movementVector;
    Vector3 position;
    Vector3 offset;
    public Text score;
    public Text record;
    float timespeed = 2;
    int _score;

    void Start()
    {
        rg = GetComponent<Rigidbody>();
        position = transform.position;
        pl.transform.localScale = new Vector3(2.7f, 0.2f, 2.7f);
        movingplatform.transform.localScale = new Vector3(2.7f, 0.2f, 2.7f);
        score.text = "0";
        _score = PlayerPrefs.GetInt("Record");
        record.text = "Record: " + _score;
        Time.timeScale = 0;
        this.transform.position = new Vector3(10f, 6f, 0f);
    }
    
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            //if (Mathf.Abs(Input.mousePosition.x / 100 + 6f - this.transform.position.x) <= 0.1)
            //Time.timeScale = timespeed;
            //if (Mathf.Abs(Input.mousePosition.x / 100 + 6f - this.gameObject.transform.position.x) < 0.1)
            //    this.transform.position = new Vector3(Input.mousePosition.x / 100 + 6f, this.transform.position.y, this.transform.position.z);
        }

        //if (i % 20 == 0)
        //{
        //    int cl = 4;// Random.Range(4, 4);
        //    if (cl == 1)
        //    {
        //        pl.GetComponent<Renderer>().materials[0].color = Color.Lerp(transform.GetComponent<Renderer>().material.color, Color.green, Mathf.PingPong(Time.time, 1));
        //        movingplatform.GetComponent<Renderer>().materials[0].color = Color.Lerp(transform.GetComponent<Renderer>().material.color, Color.green, Mathf.PingPong(Time.time, 1));
        //    }
        //    else if (cl == 2)
        //    {
        //        pl.transform.GetComponent<Renderer>().materials[0].color = Color.Lerp(transform.GetComponent<Renderer>().material.color, Color.red, changeSpeed * Time.deltaTime);
        //        movingplatform.transform.GetComponent<Renderer>().materials[0].color = Color.Lerp(transform.GetComponent<Renderer>().material.color, Color.red, changeSpeed * Time.deltaTime);
        //    }
        //    else if (cl == 3)
        //    {
        //        pl.gameObject.transform.GetComponent<Renderer>().materials[0].color = Color.Lerp(transform.GetComponent<Renderer>().material.color, Color.blue, changeSpeed * Time.deltaTime);
        //        movingplatform.transform.GetComponent<Renderer>().materials[0].color = Color.Lerp(transform.GetComponent<Renderer>().material.color, Color.blue, changeSpeed * Time.deltaTime);
        //    }
        //    else if (cl == 4)
        //    {
        //        pl.transform.GetComponent<Renderer>().sharedMaterials[0].SetColor(1, Color.Lerp(transform.GetComponent<Renderer>().material.color, Color.black, Mathf.PingPong(Time.time, 1)));
        //        movingplatform.transform.GetComponent<Renderer>().sharedMaterials[0].SetColor(1, Color.Lerp(transform.GetComponent<Renderer>().material.color, Color.black, Mathf.PingPong(Time.time, 1)));
        //    }
        //    else if (cl == 5)
        //    {
        //        pl.transform.GetComponent<Renderer>().materials[0].color = Color.Lerp(transform.GetComponent<Renderer>().material.color, Color.grey, changeSpeed * Time.deltaTime);
        //        movingplatform.transform.GetComponent<Renderer>().materials[0].color = Color.Lerp(transform.GetComponent<Renderer>().material.color, Color.grey, changeSpeed * Time.deltaTime);
        //    }

        //}
    }
    void FixedUpdate()
    {       
        transform.Rotate(new Vector3(20, 0, 0)* 15f * Time.deltaTime);
    }

    void LateUpdate()
    {
        this.transform.position = new Vector3(Input.mousePosition.x / 100 + 6f, this.transform.position.y, this.transform.position.z);
        if (Input.GetMouseButtonDown(0))
        {
            //if (Mathf.Abs(Input.mousePosition.x / 100 + 6f - this.transform.position.x) <= 0.1)
                Time.timeScale = timespeed;
            //if (Mathf.Abs(Input.mousePosition.x / 100 + 6f - this.gameObject.transform.position.x) < 0.1)
            //    this.transform.position = new Vector3(Input.mousePosition.x / 100 + 6f, this.transform.position.y, this.transform.position.z);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("up"))
        {   
            rg.velocity = Vector3.zero;
            this.transform.position = new Vector3(this.position.x, 0.02f, 0);
            movementVector = position*0.89f;
            Debug.Log(Input.mousePosition.x / 100 + 6f);
            rg.AddForce(movementVector * speed);
            i++;
            int random = Random.Range(1,10);
            if (i < 100)
            {
                if (random >= 3)
                {
                    Instantiate(pl);
                    pl.transform.position = new Vector3(Random.Range(7f, 14f), -0.6f, 27);
                }
                else if (random == 1)
                {
                    movingplatform.transform.position = new Vector3(Random.Range(7f, 8f), -0.6f, 27);
                    Instantiate(movingplatform);
                }
                else
                {
                    movingplatform.transform.position = new Vector3(Random.Range(10f, 13f), -0.6f, 27);
                    Instantiate(movingplatform);
                }
            }
            else
            {
                if (random == 1 || random == 2)
                {
                    movingplatform.transform.position = new Vector3(Random.Range(7f, 8f), -0.6f, 27);
                    Instantiate(movingplatform);
                }
                else if(random == 3 || random == 4)
                {
                    movingplatform.transform.position = new Vector3(Random.Range(10f, 13f), -0.6f, 27);
                    Instantiate(movingplatform);
                }
                else
                {
                    Instantiate(pl);
                    pl.transform.position = new Vector3(Random.Range(7f, 14f), -0.6f, 27);
                }
            }
            if (i >= 10 && i <= 20 || i >= 40 && i <= 45)
            {
                pl.transform.localScale = new Vector3(pl.transform.localScale.x - 0.05f, pl.transform.localScale.y, pl.transform.localScale.z - 0.05f);
                movingplatform.transform.localScale = new Vector3(pl.transform.localScale.x - 0.05f, pl.transform.localScale.y, pl.transform.localScale.z - 0.05f);
            }

            if ((i > 9 && i < 20) || (i > 43 && i < 55) || (i > 79 && i < 95) || (i > 100 && i < 120))
                if (Time.timeScale < 4)
                {
                    Time.timeScale += 0.1f;
                    timespeed += 0.1f;
                }          
            if(i>_score)
            {
                record.text = "NEW RECORD!";
                PlayerPrefs.SetInt("Record", i);
            }
            score.text = i.ToString();
                       
        }
    }
}
